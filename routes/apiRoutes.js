const express = require("express");
const router = express.Router();

const getEmployees = require('../action/getEmployees');
const addEmployee = require('../action/addEmployee');
const editEmployee = require('../action/editEmployee');
const deleteEmployees = require('../action/deleteEmployees');

router.get('/get-employees', (req, res) => {
    console.log("Request: get-employees");

    let employees = getEmployees.getAllEmployees();
    res.send(employees);
});

router.post('/add-employee', (req, res) => {
    console.log("Request: add-employee");
    let employee = req.body;

    let response = addEmployee.addEmployee(employee);
    res.send(response);
});
router.post('/edit-employee/:employeeIndex', (req, res) => {
    console.log("Request: edit-employee");
    let employeeIndex = req.params.employeeIndex;
    let employee = req.body;

    let response = editEmployee.editEmployee(employee,employeeIndex);
    res.send(response);
});

router.post('/delete-employees', (req, res) => {
    console.log("Request: delete employees");
    let employeesIndex = req.body.employeesIndex;

    let response = deleteEmployees.deleteEmployees(employeesIndex);
    res.send(response);
});

module.exports = router;