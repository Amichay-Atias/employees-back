const express = require('express');
const app = express();
const port = 3000;

/* cors */
const cors = require('cors');
app.use(cors());
app.use((req, res, next) => {
    res.append('Access-Control-Allow-Headers', 'Content-Type');
    next();
})

/* create server */
var httpProtocol = require('http').Server(app);
httpProtocol.listen(port, () => {
    console.log(`Listening on port ${port}`);
});


/* use restAPIs */
app.use(express.json()); // define the use of the return format, here we use json.
app.use('/api',  require('./routes/apiRoutes'));