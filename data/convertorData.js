
function convertJsonToObject(data) {
    try {
        return JSON.parse(data);
    }
    catch (ex) {
        console.log("Failed convert employees object");
        return {};
    }
}


function convertObjectToJson(data) {
    try {
        return JSON.stringify(data);
    }
    catch (ex) {
        console.log("Failed convert employees object");
        return {};
    }
}

module.exports.convertJsonToObject = convertJsonToObject;
module.exports.convertObjectToJson = convertObjectToJson;