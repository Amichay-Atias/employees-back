const fs = require('fs');
const filePath = "./data/employees.json";
const convertor = require('./convertorData');


function readFile() {
    try {
        let employees = fs.readFileSync(filePath);
        return convertor.convertJsonToObject(employees);
    }
    catch (ex) {
        fs.writeFileSync(filePath, '{}');
        return {};
    }
}

function writeFile(employees) {
    try {
        employees = convertor.convertObjectToJson(employees);
        fs.writeFileSync(filePath, employees);
        return true;
    }
    catch (ex) {
        return false;
    }
}

module.exports.readFile = readFile;
module.exports.writeFile = writeFile;