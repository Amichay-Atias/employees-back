const http = require('http');
const express = require('express')
const cors = require('cors');
const bodyParser = require('body-parser');
const app = express();

const hostname = '127.0.0.1';
const port = 3000;

const getEmployees = require('./action/getEmployees');


// allow access from other services
app.use(cors());
app.use((req, res, next) => {
  res.append('Access-Control-Allow-Headers', 'Content-Type');
  next();
})

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));


/* API  */
app.get('/', (req, res) => {
  res.send('Hello World!')
});

app.get('api/get-employees', (req, res) => {
  console.log("Request: get-employees");

  let employees = getEmployees.getAllEmployees();
  res.send(employees);
});

app.get('api/add-employee', (req, res) => {
  console.log("Request: add-employee");
  res.send('Hello World!')
});

app.get('api/delete-employees', (req, res) => {
  console.log("Request: delete employees");
  res.send('Hello World!')
});


/* Server */
const server = http.createServer((req, res) => {
  res.statusCode = 200;
  res.setHeader('Content-Type', 'text/plain');
  res.end('Hello World');
});

server.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/`);
});