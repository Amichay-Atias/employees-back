const fs = require('fs');
const getEmployees = require('./getEmployees');
const manageFile = require('../data/manageFile');



function addEmployee(employee) {
    let employees = getEmployees.getAllEmployees();

    employees = addEmployeeToList(employees, employee);

    let response = manageFile.writeFile(employees);

    return response;
}

function addEmployeeToList(employees, employee) {

    let employeeIds = Object.keys(employees);
    newIndex = getTheNextIndex(employeeIds);

    employees[newIndex] = employee;
    return employees;
}

function getTheNextIndex(employeeIds) {
    let newIndex = Number(employeeIds[employeeIds.length - 1]) + 1;
    return newIndex;
}

module.exports.addEmployee = addEmployee;