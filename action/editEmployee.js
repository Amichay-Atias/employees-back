
const fs = require('fs');
const getEmployees = require('./getEmployees');
const manageFile = require('../data/manageFile');


function editEmployee(employee, employeeIndex) {
    let employees = getEmployees.getAllEmployees();

    employees = updateEmployeesList(employees, employee, employeeIndex);

    let response = manageFile.writeFile(employees);

    return response;
}

function updateEmployeesList(employees, employee, employeeIndex) {
    delete employees[employeeIndex];
    employees[employeeIndex] = employee;

    return employees;
}

module.exports.editEmployee = editEmployee;