const fs = require('fs');
const getEmployees = require('./getEmployees');
const manageFile = require('../data/manageFile');


function deleteEmployees(employeesIndex) {
    let employees = getEmployees.getAllEmployees();

    employees = findAndRemoveEmployees(employees, employeesIndex);

    let response = manageFile.writeFile(employees);

    return response;
}

function findAndRemoveEmployees(employees, employeesIndex) {

    let arr = employeesIndex.split(',');
    for (let employeeIndex of arr) {
        for (let index of Object.keys(employees)) {
            if (employeeIndex == index) {
                delete employees[index];
                break;
            }
        }
    }

    return employees;
}

module.exports.deleteEmployees = deleteEmployees;