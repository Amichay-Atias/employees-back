const fs = require('fs');
const manageFile = require('../data/manageFile');


function getAllEmployees() {
    let employees = manageFile.readFile();
    return employees;
}

module.exports.getAllEmployees = getAllEmployees;